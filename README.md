This file contains instructions on using GIT.

# How to create repository
1. Login to bitbucket
2. Find 'Create' button on top menu
3. Enter required information and create.

# Your First commit

This is first step to add some files and commit to bitbucket

1. create directoy (mkdir _name_)
2. cd _name_
3. git init
4. git remote add origin _url_of_git_
5. Add files
6. git add .
7. git commit -m "Your message here"
8. git push -u origin master
9. Enter password (bitbucket password)

# Update or Add files and folders

1. Add files and folder as you wanted then enter commands on Terminal
2. git add . (. for all, * for recursively and 'name of file' for particular file)
3. git commit -m "Your message here to know exactly what was done in this version"
4. git push -u origin master
5. enter password (bitbucket password)

